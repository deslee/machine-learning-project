The cat caught the mouse because it was slow.
it
The cat, the mouse
the mouse

The cat caught the mouse because it was fast.
it
The cat, the mouse
The cat

John brought Bill dinner because he was generous.
he
John, Bill
John

John brought Bill dinner because he was hungry.
he
John, Bill
Bill

Mary yelled at Sue because she was late.
she
Mary, Sue
Sue

Mary yelled at Sue because she was angry.
she
Mary, Sue
Mary

Mary let Sue have the book because she enjoys reading.
she
Mary, Sue
Sue

Mary let Sue have the book because she dislikes reading.
she
Mary, Sue
Mary

John let Bill drive because he dislikes driving.
he
John, Bill
John

John let Bill drive because he enjoys driving.
he
John, Bill
Bill

John ran faster than Bill because he hated running.
he
John, Bill
Bill

John ran faster than Bill because he loved running.
he
John, Bill
John

Mary borrowed Sue's stapler after she asked for the it.
she
Mary, Sue
Mary

Mary borrowed Sue's stapler after she offered it. 
she
Mary, Sue
Sue

Mary gave Sue some paper because she had none.
she
Mary, Sue
Mary

Mary gave Sue some paper because she had plenty.
she
Mary, Sue
Sue

John gave Bill a thank-you note because he was helpful.
he
John, Bill
Bill

John gave Bill a thank-you note because he was helped.
he
John, Bill
John

Bill paid for John's dinner because he was wealthy.
he
Bill, John
Bill

Bill paid for John's dinner because he was poor.
he
Bill, John
John
