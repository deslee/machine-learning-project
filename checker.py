#!/bin/python3
import sys

if __name__ == '__main__':
  fname = sys.argv[1]
  f = open(fname, 'r')
  str = f.read()
  arr = str.split('\n');

  set = []
  
  for i in range(0,len(arr), 10):
    if len(arr) - i >= 10:
      pair = []
      for j in [0,5]:
        sentence = []
        for k in range(4):
          sentence.append(arr[i+j+k])
        pair.append(sentence)
      set.append(pair)

  print(set)
